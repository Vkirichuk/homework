function add(a, b) {
    return a + b;
}

function sub(a, b) {
    return a - b;
}

function mul(a, b) {
    return a * b;
}

function div(a, b) {
    if (b == 0) {
        document.write("Error")
    }
    else return a / b;
}

const operand1 = +prompt("Введите первое число: "),
    sign = prompt("Введите знак арифметической операции: + - * /"),
    operand2 = +prompt("Введите второе число: ");
let result;

switch (sign) {
    case "+":
        result = add(operand1, operand2);
        break;
    case "-":
        result = sub(operand1, operand2);
        break;
    case "*":
        result = mul(operand1, operand2);
        break;
    case "/":
        result = div(operand1, operand2);
        break;
    default:
        document.write("<p><b style='color:red'>" + sign + "</b> - не является знаком арифметической операции.");
}

if (result != undefined)
    document.write("<p>" + operand1 + " " + sign + " " + operand2 + " = " + result);
